<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Estudiantes;
use DB;

class EstudiantesController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
        $estudiantes = Estudiantes::get();
        return view('Estudiantes/list')->with('estudiantes', $estudiantes);
    }

    public function getGrados(){
        $grados = DB::table('cursos')
            ->select('*')
            ->get();
        return view('Estudiantes/crear')->with('grados', json_decode($grados,true));;
    }

    public function detalle(Request $request){
        $detalles = DB::table('estudiantes as e')
            ->select('m.nombre_materia','n.nota1','n.nota2','n.nota3','n.Total')
            ->join('notas as n', 'n.id_estudiante', '=', 'e.id')
            ->join('materias as m', 'm.id', '=', 'n.id_materia')
            ->where('e.id', $request->id)
            ->get();
        return view('Estudiantes/detalle')->with('detalles', $detalles);
    }


    public function create(Request $request){

        $estudiante = new Estudiantes;

        $estudiante->primer_nombre = $request->primer_nombre;
        $estudiante->primer_apellido = $request->primer_apellido;
        $estudiante->identificacion = $request->identificacion;
        $estudiante->id_curso = $request->id_curso;

        $estudiante->save();

        return $this->index();
    }

    public function edit(Request $request){
        $estudiantes = Estudiantes::findOrFail($request->id);

        return view('Estudiantes/editar')->with('estudiantes', $estudiantes);
    }

    public function update(Request $request){

        $estudiante = Estudiantes::findOrFail($request->id);

        $estudiante->primer_nombre = $request->primer_nombre;
        $estudiante->primer_apellido = $request->primer_apellido;
        $estudiante->identificacion = $request->identificacion;
        $estudiante->save();

        return $this->index();
    }

    public function destroy(Request $request){
        $estudiante = Estudiantes::findOrFail($request->id);
        $estudiante->delete();

        return $this->index();
    }
}
