<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;

class ApiRestController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getRequest(){
        $personas = [];
        $allCaracteres = "";
        $allCaracteresUsed;
        $mayorCaracter = "";
        $countCaracter = 0;
        $contador=0;

        /* usar el servicio 5 veces */
        for ($i = 0; $i <5 ; $i++) {
            $response = Http::get('https://randomuser.me/api/');
            $data = $response->json();
            if(!empty($data['results'][0]['name']['first']) && isset($data['results'][0]['name']['first'])){
                /* validacion utf8 para algunos nombres */
                $personas[$i]= mb_convert_encoding($data['results'][0]['name']['first'], 'UTF-8', 'UTF-8').mb_convert_encoding($data['results'][0]['name']['last'], 'UTF-8', 'UTF-8');
            }
        }
        /* creando un string de todos los nombres */
        foreach ($personas as $item) {
            /* fragmentar por nombre */
            $calculoString = str_split($item);
            $num = count($calculoString);
            for ($i = 0; $i < $num; ++$i){
                $allCaracteres =$allCaracteres.$calculoString[$i];
            }
        }
        /* validar que no venga vacio o no existe */
        if(!empty($allCaracteres) && isset($allCaracteres)){
            /* quitar los espacios */
            $allCaracteres = str_replace(" ", "", $allCaracteres);
            /* converit a minusculas */
            $allCaracteres = strtolower($allCaracteres);
            /* descomponer y comparar el caracter mas usado */
            $allCaracteresUsed = str_split($allCaracteres);
            $num_allCaracteres = count($allCaracteresUsed);
            /* descomponer toda la frase y contar caracteres */
            for ($i = 0; $i < $num_allCaracteres; ++$i){
                $contador = substr_count($allCaracteres, $allCaracteresUsed[$i]);
                if($contador>$countCaracter){
                    $mayorCaracter = $allCaracteresUsed[$i];
                    $countCaracter = $contador;
                }
            }
        }else{
            $mayorCaracter="Error en los nombres vuelve a correr el Servicio";
        }
        /* respuesta en json */
        $res = [
            "5 nombres Al azar" =>$personas,
            "Letra mas utilizada"=>$mayorCaracter,
            "Veces que se repitio en los nombres"=>$countCaracter
        ];

        return $res;
    }
}
