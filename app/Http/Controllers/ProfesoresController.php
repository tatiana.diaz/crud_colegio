<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Profesores;
use App\Models\Asociacion;
use DB;

class ProfesoresController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
        $profesores = Profesores::get();
        return view('Profesores/list')->with('profesores', $profesores);
    }

    public function getAsociacion(){
        $materias = DB::table('materias')
            ->select('*')
            ->get();
        $profesores = DB::table('profesores')
            ->select('*')
            ->get();
        
        $relacion = DB::table('profesores as p')
            ->select('*')
            ->join('profesores_materias as pm', 'pm.id_profesores', '=', 'p.id')
            ->join('materias as m', 'm.id', '=', 'pm.id_materias')
            ->get();

        return view('Profesores/asociacion')
                ->with('materias', json_decode($materias,true))
                ->with('profesores', json_decode($profesores,true))
                ->with('relaciones', json_decode($relacion,true));
    }

    public function putAsociacion(Request $request){

        $nota = new Asociacion;

        $nota->id_materias = $request->id_materias;
        $nota->id_profesores = $request->id_profesores;
        $nota->save();

        return redirect()->back();
    }

    public function create(Request $request){

        $profesores = new Profesores;

        $profesores->nombre_profesor = $request->nombre_profesor;

        $profesores->save();

        return $this->index();
    }

    public function edit(Request $request){
        $profesores = Profesores::findOrFail($request->id);

        return view('Profesores/editar')->with('profesores', $profesores);
    }

    public function update(Request $request){

        $profesores = Profesores::findOrFail($request->id);

        $profesores->nombre_profesor = $request->nombre_profesor;

        $profesores->save();

        return $this->index();
    }

    public function destroy(Request $request){
        $profesores = Profesores::findOrFail($request->id);
        $profesores->delete();

        return $this->index();
    }
}
