<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Notas;
use App\Models\Estudiantes;
use DB;

class NotasController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
        $estudiantes = Estudiantes::get();
        return view('Notas/calificacion')->with('estudiantes', $estudiantes);
    }

    public function materiasById(Request $request){
        
        $materias = DB::table('Estudiantes as e')
            ->select('m.id','m.nombre_materia')
            ->join('cursos as c', 'c.id', '=', 'e.id_curso')
            ->join('materias as m', 'm.id_curso', '=', 'c.id')
            ->where('e.id', $request->id)
            ->get();
            
        return $materias;
    }

    public function create(Request $request){

        $nota = new Notas;

        $nota->nota1 = $request->nota1;
        $nota->nota2 = $request->nota2;
        $nota->nota3 = $request->nota3;
        $nota->Total = $request->Total;
        $nota->id_estudiante = $request->id_estudiante;
        $nota->id_materia = $request->id_materia;

        $nota->save();

        return view('index');
    }

    public function edit(Request $request){
        $estudiantes = Notas::findOrFail($request->id);

        return view('Estudiantes/editar')->with('estudiantes', $estudiantes);
    }

    public function update(Request $request){

        $estudiante = Notas::findOrFail($request->id);

        $estudiante->primer_nombre = $request->primer_nombre;
        $estudiante->primer_apellido = $request->primer_nombre;
        $estudiante->identificacion = $request->identificacion;
        $estudiante->save();

        return $this->index();
    }
}
