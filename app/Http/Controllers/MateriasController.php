<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Materias;

class MateriasController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
        $materias = Materias::select('materias.id', 'materias.nombre_materia','cursos.grado')
        ->leftjoin('cursos', 'cursos.id', '=', 'materias.id_curso')
        ->get();
        return view('materias/list')->with('materias', $materias);
    }

}
