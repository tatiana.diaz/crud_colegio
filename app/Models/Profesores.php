<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Profesores extends Authenticatable
{
    protected $table = "profesores";
    # No queremos que ponga updated_at ni created_at
    public $timestamps = false;
}
