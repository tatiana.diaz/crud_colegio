<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/index', function () {
    return view('index');
});
Route::get('/', function () {
    return view('index');
});

/* Rutas de estudiante */

Route::get('/estudiantes', "App\Http\Controllers\EstudiantesController@index");

Route::get("/estudiante/crear","App\Http\Controllers\EstudiantesController@getGrados");

Route::get("/detalleEstudiante/{id}","App\Http\Controllers\EstudiantesController@detalle")
->name('detalleEstudiante');

Route::post("GuardarEstudiante", "App\Http\Controllers\EstudiantesController@create")
    ->name('GuardarEstudiante');

Route::get("/editarEstudiante/{id}", "App\Http\Controllers\EstudiantesController@edit")
    ->name("editarEstudiante");

Route::post("Actualizacion", "App\Http\Controllers\EstudiantesController@update")
    ->name("Actualizacion");


Route::get("/eliminarEstudiante/{id}", "App\Http\Controllers\EstudiantesController@destroy")
    ->name("eliminarEstudiante");

/* Rutas de materias */

Route::get('/materias', "App\Http\Controllers\MateriasController@index");


/* Rutas de Profesores */

Route::get('/profesores', "App\Http\Controllers\ProfesoresController@index");

Route::get("/profesores/crear",function () {
    return view('Profesores/crear');
});

Route::get('/profesores/asociacion', "App\Http\Controllers\ProfesoresController@getAsociacion");

Route::post("GuardarProfesor", "App\Http\Controllers\ProfesoresController@create")
    ->name('GuardarProfesor');

Route::get("/editarProfesor/{id}", "App\Http\Controllers\ProfesoresController@edit")
    ->name("editarProfesor");

Route::post("ActualizacionProfesor", "App\Http\Controllers\ProfesoresController@update")
    ->name("ActualizacionProfesor");


Route::get("/eliminarProfesor/{id}", "App\Http\Controllers\ProfesoresController@destroy")
    ->name("eliminarProfesor");

Route::post("asociacion", "App\Http\Controllers\ProfesoresController@putAsociacion")
    ->name('asociacion');

/* Rutas de Notas */

Route::get('/notas', "App\Http\Controllers\NotasController@index");


Route::post("Calificar", "App\Http\Controllers\NotasController@create")
    ->name('Calificar');



