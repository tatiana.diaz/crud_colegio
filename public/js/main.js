$( document ).ready(function() {

    $("#id_estudiante" ).change(function () {
        let value = $( "#id_estudiante option:selected" ).val();
        $.ajax({
            url:"api/buscarByID",
            type:"POST",
            datatype: "json",
            data: {id:value}, 
            success:function(data){
                $("#id_materia").empty();
                data.forEach(element => {
                        $("#id_materia").append('<option value="'+element.id+'">'+element.nombre_materia+'</option>');
                    });
                }
            }); 
    })

});
