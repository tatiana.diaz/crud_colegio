@extends("header")
@section("titulo", "Calificacion")

@section("contenido")
<div class="main">
    <div class="tk_content">
        <div class="tk_box">
            <div class="tk_form">
                <h1>Calificacion</h1>
                <br>
                <form method="POST" action="{{ route('Calificar') }}" >
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="id_estudiante">Estudiante a calificar</label>
                                <select class="form-control" name="id_estudiante" id="id_estudiante" required>
                                    <option value="">Seleccione...</option>
                                    @foreach ($estudiantes as $item)
                                        <option value="{{ $item['id'] }}">{{$item['primer_nombre'].' '.$item['primer apellido']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="id_materia">Materias de Estudiante</label>
                                <select class="form-control" name="id_materia" id="id_materia">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="nota1">Nota 1</label>
                                <input type="number" step="0.01" min="1" max="5" class="form-control" id="nota1" value ="0" name="nota1" placeholder="Nota" onKeyUp="calculate(this)">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="nota2">Nota 2</label>
                                <input type="number" step="0.01" min="1" max="5" class="form-control" id="nota2" value ="0" name="nota2" placeholder="Nota" onKeyUp="calculate(this)" >
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="nota3">Nota 3</label>
                                <input type="number" step="0.01" min="1" max="5" class="form-control" id="nota3" value ="0" name="nota3" placeholder="Nota" onKeyUp="calculate(this)">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Total">Definitiva</label>
                                <input type="number" step="0.01" class="form-control" id="Total" name="Total" placeholder="Nota Total" readonly>
                                <label for="nota">Se pasa sobre 2.9</label>
                                <div>
                                    <input class="form-control" for="evaluacion" id="evaluacion" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary d-flex float-right">Calificar</button>
                    
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function calculate(){
        let value1 = parseFloat($('#nota1').val());
        let value2 = parseFloat($('#nota2').val());
        let value3 = parseFloat($('#nota3').val());
        let total = 0;
        
        total = (value1+value2+value3)/3;
        
        $('#Total').val(total);

        if(parseFloat($('#Total').val())>=3.0 || parseFloat($('#Total').val())>=3){
            $('#evaluacion').val('Pasa')
        }else{
            $('#evaluacion').val('No pasa')
        }
    }
</script>
@endsection
