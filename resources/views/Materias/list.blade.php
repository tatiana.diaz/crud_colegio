@extends("header")
@section("titulo", "Materias")

@section("contenido")
<div class="main">
    <div class="tk_content">
        <h1>Lista Materias</h1> 
        <br>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Curso</th>
                </tr>
            </thead>
            <tbody>
                <!-- contador para los materias -->
            @foreach ($materias as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->nombre_materia }}</td>
                <td>{{ $item->grado }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
