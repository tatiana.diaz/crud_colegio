@extends("header")
@section("titulo", "Editar Profesor")

@section("contenido")

<div class="main">
    <div class="tk_content">
        <div class="tk_box">
            <div class="tk_form">
                <h1>Editar Profesor</h1>
                <br>
                <form method="POST" action="{{ route('ActualizacionProfesor') }}" >
                    @csrf
                    <input type="hidden" value="{{ $profesores->id }}" name="id">
                    <div class="form-group">
                        <label for="nombre_profesor">Nombre</label>
                        <input type="text" class="form-control" id="nombre_profesor" name="nombre_profesor" value="{{ $profesores->nombre_profesor }}" placeholder="Nombre" required>
                    </div>
                    <button type="submit" class="btn btn-primary d-flex float-right">Actualizar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
