@extends("header")
@section("titulo", "Inicio")

@section("contenido")
<div class="main">
    <div class="tk_content">
        <h1>Asociar Profesor con Grado</h1> 
        <br>
        <form method="POST" action="{{ route('asociacion') }}" >
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="id_profesores">Profesor</label>
                        <select class="form-control" name="id_profesores" id="id_profesores" required>
                            <option value="">Seleccione...</option>
                            @foreach ($profesores as $item)
                                <option value="{{ $item['id'] }}">{{$item['nombre_profesor']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                    <label for="id_materias">Materia</label>
                    <select class="form-control" name="id_materias" id="id_materias" required>
                        <option value="">Seleccione...</option>
                        @foreach ($materias as $item)
                            <option value="{{ $item['id'] }}">{{$item['nombre_materia']}}</option>
                        @endforeach
                    </select>
                </div>
                <div style="padding-bottom: 20%;">
                    <button type="submit" class="btn btn-primary d-flex float-right">Asociar Profesores</button>
                </div>
                <br>
            </div>
        </form>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Nombre Profesor</th>
                            <th>Materia</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- contador para los estudiantes -->
                    <input type="hidden" value="{{ $contador = 1 }}">
                    @foreach ($relaciones as $item)
                    <tr>
                        <td>{{ $contador++ }}</td>
                        <td>{{ $item['nombre_profesor'] }}</td>
                        <td>{{ $item['nombre_materia'] }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
