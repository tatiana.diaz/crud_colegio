@extends("header")
@section("titulo", "Crear")

@section("contenido")
<div class="main">
    <div class="tk_content">
        <div class="tk_box">
            <div class="tk_form">
                <h1>Crear un Profesor</h1>
                <br>
                <form method="POST" action="{{ route('GuardarProfesor') }}" >
                    @csrf
                    <div class="form-group">
                        <label for="nombre_profesor">Nombre Completo</label>
                        <input type="text" class="form-control" id="nombre_profesor" name="nombre_profesor" placeholder="Nombre" required>
                    </div>
                    <button type="submit" class="btn btn-primary d-flex float-right">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
