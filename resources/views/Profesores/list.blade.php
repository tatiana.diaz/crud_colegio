@extends("header")
@section("titulo", "Inicio")

@section("contenido")
<div class="main">
    <div class="tk_content">
        <h1>Lista Estudiantes</h1> 
        <br>
        <div>
            <a href="{{ URL::to('/') }}/profesores/crear" class="btn btn-success">Crear</a>
        </div>
        <br>
        <table class="table table-striped table-responsive">
            <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <!-- contador para los estudiantes -->
            <input type="hidden" value="{{ $contador = 1 }}">
            @foreach ($profesores as $item)
            <tr>
                <td>{{ $contador++ }}</td>
                <td>{{ $item->nombre_profesor }}</td>
                <td>
                    <a href="{{ route('editarProfesor', ['id' => $item->id]) }}" class="btn btn-warning">Editar</a>
                    <a href="{{ route('eliminarProfesor', ['id' => $item->id]) }}" class="btn btn-danger">Eliminar</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
