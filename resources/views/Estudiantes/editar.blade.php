@extends("header")
@section("titulo", "Editar Estudiante")
@section("contenido")

<div class="main">
    <div class="tk_content">
        <div class="tk_box">
            <div class="tk_form">
                <h1>Editar Estudiante</h1>
                <br>
                <form method="POST" action="{{ route('Actualizacion') }}" >
                    @csrf
                    <input type="hidden" value="{{ $estudiantes->id }}" name="id">
                    <div class="form-group">
                        <label for="primer_nombre">Nombre</label>
                        <input type="text" class="form-control" id="primer_nombre" name="primer_nombre"  value="{{ $estudiantes->primer_nombre }}" placeholder="Primer nombre" required>
                    </div>
                    <div class="form-group">
                        <label for="primer_apellido">Apellido</label>
                        <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" value="{{ $estudiantes->primer_apellido }}" placeholder="Primer Apellido" required>
                    </div>
                    <div class="form-group">
                        <label for="identificacion">Identificacion</label>
                        <input type="number" class="form-control" id="identificacion" name="identificacion" value="{{ $estudiantes->identificacion }}" placeholder="Identificacion" required>
                    </div>
                    <button type="submit" class="btn btn-primary d-flex float-right">Actualizar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
