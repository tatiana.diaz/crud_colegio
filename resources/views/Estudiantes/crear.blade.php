@extends("header")
@section("titulo", "Crear")

@section("contenido")
<div class="main">
    <div class="tk_content">
        <div class="tk_box">
            <div class="tk_form">
                <h1>Crear un Estudiante</h1>
                <br>
                <form method="POST" action="{{ route('GuardarEstudiante') }}" >
                    @csrf
                    <div class="form-group">
                        <label for="primer_nombre">Nombre</label>
                        <input type="text" class="form-control" id="primer_nombre" name="primer_nombre" aria-describedby="emailHelp" placeholder="Primer nombre" required>
                    </div>
                    <div class="form-group">
                        <label for="primer_apellido">Apellido</label>
                        <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" placeholder="Primer apellido" required>
                    </div>
                    <div class="form-group">
                        <label for="identificacion">Identificacion</label>
                        <input type="number" class="form-control" id="identificacion" name="identificacion" placeholder="Identificacion" required>
                    </div>
                    <div class="form-group">
                        <label for="id_curso">Grado</label>
                        <select class="form-control" name="id_curso" id="id_curso" required>
                                <option value="">Seleccione...</option>
                                @foreach ($grados as $item)
                                    <option value="{{ $item['id'] }}">{{$item['grado']}}</option>
                                @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary d-flex float-right">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
