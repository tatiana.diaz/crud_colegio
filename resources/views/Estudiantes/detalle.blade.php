@extends("header")
@section("titulo", "Inicio")


@section("contenido")
<div class="main">
    <div class="tk_content">
        <h1>Detalle de Estudiante</h1> 
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>Materia</th>
                    <th>Nota 1</th>
                    <th>Nota 2</th>
                    <th>Nota 3</th>
                    <th>Definitiva</th>
                </tr>
            </thead>
            <tbody>
                <!-- contador para los conteo -->
            <input type="hidden" value="{{ $contador = 1 }}">
            @foreach ($detalles as $item)
            <tr>
                <td>{{ $contador++ }}</td>
                <td>{{ $item->nombre_materia }}</td>
                <td>{{ $item->nota1 }}</td>
                <td>{{ $item->nota2 }}</td>
                <td>{{ $item->nota3 }}</td>
                <td>{{ $item->Total }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
