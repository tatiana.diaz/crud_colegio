@extends("header")
@section("titulo", "Inicio")
@section("contenido")

<div class="main">
    <div class="tk_content">
        <h1>Lista Estudiantes</h1> 
        <br>
        <div>
            <a href="{{ URL::to('/') }}/estudiante/crear" class="btn btn-success">Crear</a>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-responsive">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Identificacion</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- contador para los estudiantes -->
                    <input type="hidden" value="{{ $contador = 1 }}">
                    @foreach ($estudiantes as $item)
                    <tr>
                        <td>{{ $contador++ }}</td>
                        <td>{{ $item->primer_nombre }}</td>
                        <td>{{ $item->primer_apellido }}</td>
                        <td>{{ $item->identificacion }}</td>
                        <td>
                            <a href="{{ route('detalleEstudiante', ['id' => $item->id]) }}" class="btn btn-info">Detalle</a>
                            <a href="{{ route('editarEstudiante', ['id' => $item->id]) }}" class="btn btn-warning">Editar</a>
                            <a href="{{ route('eliminarEstudiante', ['id' => $item->id]) }}" class="btn btn-danger">Eliminar</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
